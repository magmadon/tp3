from tp2 import *

def test_box_create():
    b = Box()
    b.add("truc1")
    b.add("truc2")

def test_box_in():
    b = Box()
    b.add("truc")
    b.add("machin")
    assert "truc" in b
    assert "machin" in b

def test_box_remove():
    b = Box()
    b.add("truc")
    b.add("machin")
    b.retirer("truc")
    b.retirer("machin")

def test_close():
    b = Box()
    b.close()
    b.is_open()
    b.open()
    b.is_open()

def test_action_regarder():
    b = Box()
    b.add("ceci")
    b.add("cela")
    b.open()
    assert b.action_look()=="La boite contient: ceci, cela"

def test_thing_create():
    t = Thing(3)

def test_thing_volume():
    t = Thing(3)
    t.volume()

def test_box_capacity():
    b = Box()
    b.set_capacity(5)
    b.capacity()

def reste_place():
    b = Box(5)
    t = Thing(3)
    assert b.has_room_for(t)
#reste_place()
#bite
