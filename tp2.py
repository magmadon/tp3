class Box:
    def __init__(self,capacity=None):
        self._contents = []
        self._etat="close"
        self._espace=capacity

    def add(self, truc):
        self._contents.append(truc)

    def __contains__(self, machin):
        return machin in self._contents

    def retirer(self, truc):
        self._contents.remove(truc)

    def close(self):
        if self._etat!="close":
            self._etat="close"

    def open(self):
        if self._etat!="open":
            self._etat="open"

    def is_open(self):
        if self._etat=="open":
            return True
        else:
            return False

    def action_look(self):
        if self.is_open():
            return "La boite contient: "+", ".join(self._contents)
        else:
            return "La boite est fermée"

    def set_capacity(self, capacity):
        self._espace=capacity

    def capacity(self):
        return self._espace

    def has_room_for(self, objet):
        if self._espace>objet or self._espace==None:
            return True
        else:
            return False

class Thing:
    def __init__(self, espace):
        self._taille=espace

    def volume(self):
        return self._taille
